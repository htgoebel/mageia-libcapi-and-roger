=========================
libcapi20-3 für Mageia
=========================

Building
-------------

::

  git checkout libcapi
  rpmbuild -ba --define='%_topdir '"$PWD" SPECS/libcapi20-3.spec

  git checkout roger
  rpmbuild -ba --define='%_topdir '"$PWD" SPECS/roger.spec


Updating to a new release
-----------------------------

Update the upstream/venodr branch::

  url="http://download.opensuse.org/repositories/home:/tabos-team:"
  url="$url"/release/Fedora_20/src

  git checkout upstream
  git rm SOURCES/*
  rpm -i --nosignature --define='%_topdir '"$PWD" \
      "$url"/libcapi20-3-20120610-234.1.src.rpm
      "$url"/roger-1.8.2-23.1.src.rpm
  git add SOURCES/*
  git commit -m "Updating to upsearch version XXX."

Now new sources are in place, rebase our changes on the new source ...::

  git rebase upstream libcapi

... and update the .spec file as required, esp. mind the version.
